package com.tech.buddy.service;

import com.tech.buddy.domain.BrandsCategoryId;
import com.tech.buddy.dto.BrandsCategoryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface BrandsCategoryService {
    BrandsCategoryDTO save(BrandsCategoryDTO dto);

    void save(List<BrandsCategoryDTO> dtos);

    void deleteById(BrandsCategoryId id);

    Optional<BrandsCategoryDTO> findById(BrandsCategoryId id);

    List<BrandsCategoryDTO> findAll();

    Page<BrandsCategoryDTO> findAll(Pageable pageable);

    BrandsCategoryDTO updateById(BrandsCategoryDTO dto);
}