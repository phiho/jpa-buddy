package com.tech.buddy.service;

import com.tech.buddy.dto.ReviewsVoteDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ReviewsVoteService {
    ReviewsVoteDTO save(ReviewsVoteDTO dto);

    void save(List<ReviewsVoteDTO> dtos);

    void deleteById(Integer id);

    Optional<ReviewsVoteDTO> findById(Integer id);

    List<ReviewsVoteDTO> findAll();

    Page<ReviewsVoteDTO> findAll(Pageable pageable);

    ReviewsVoteDTO updateById(ReviewsVoteDTO dto);
}