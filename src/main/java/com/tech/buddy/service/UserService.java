package com.tech.buddy.service;

import com.tech.buddy.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserService {
    UserDTO save(UserDTO dto);

    void save(List<UserDTO> dtos);

    void deleteById(Integer id);

    Optional<UserDTO> findById(Integer id);

    List<UserDTO> findAll();

    Page<UserDTO> findAll(Pageable pageable);

    UserDTO updateById(UserDTO dto);
}