package com.tech.buddy.service;

import com.tech.buddy.domain.UsersRoleId;
import com.tech.buddy.dto.UsersRoleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UsersRoleService {
    UsersRoleDTO save(UsersRoleDTO dto);

    void save(List<UsersRoleDTO> dtos);

    void deleteById(UsersRoleId id);

    Optional<UsersRoleDTO> findById(UsersRoleId id);

    List<UsersRoleDTO> findAll();

    Page<UsersRoleDTO> findAll(Pageable pageable);

    UsersRoleDTO updateById(UsersRoleDTO dto);
}