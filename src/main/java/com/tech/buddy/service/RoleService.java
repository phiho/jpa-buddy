package com.tech.buddy.service;

import com.tech.buddy.dto.RoleDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface RoleService {
    RoleDTO save(RoleDTO dto);

    void save(List<RoleDTO> dtos);

    void deleteById(Integer id);

    Optional<RoleDTO> findById(Integer id);

    List<RoleDTO> findAll();

    Page<RoleDTO> findAll(Pageable pageable);

    RoleDTO updateById(RoleDTO dto);
}