package com.tech.buddy.service;

import com.tech.buddy.dto.AddressDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface AddressService {
    AddressDTO save(AddressDTO dto);

    void save(List<AddressDTO> dtos);

    void deleteById(Integer id);

    Optional<AddressDTO> findById(Integer id);

    List<AddressDTO> findAll();

    Page<AddressDTO> findAll(Pageable pageable);

    AddressDTO updateById(AddressDTO dto);
}