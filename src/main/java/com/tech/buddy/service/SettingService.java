package com.tech.buddy.service;

import com.tech.buddy.dto.SettingDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface SettingService {
    SettingDTO save(SettingDTO dto);

    void save(List<SettingDTO> dtos);

    void deleteById(String id);

    Optional<SettingDTO> findById(String id);

    List<SettingDTO> findAll();

    Page<SettingDTO> findAll(Pageable pageable);

    SettingDTO updateById(SettingDTO dto);
}