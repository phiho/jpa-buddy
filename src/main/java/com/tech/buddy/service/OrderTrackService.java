package com.tech.buddy.service;

import com.tech.buddy.dto.OrderTrackDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface OrderTrackService {
    OrderTrackDTO save(OrderTrackDTO dto);

    void save(List<OrderTrackDTO> dtos);

    void deleteById(Integer id);

    Optional<OrderTrackDTO> findById(Integer id);

    List<OrderTrackDTO> findAll();

    Page<OrderTrackDTO> findAll(Pageable pageable);

    OrderTrackDTO updateById(OrderTrackDTO dto);
}