package com.tech.buddy.service;

import com.tech.buddy.dto.StateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface StateService {
    StateDTO save(StateDTO dto);

    void save(List<StateDTO> dtos);

    void deleteById(Integer id);

    Optional<StateDTO> findById(Integer id);

    List<StateDTO> findAll();

    Page<StateDTO> findAll(Pageable pageable);

    StateDTO updateById(StateDTO dto);
}