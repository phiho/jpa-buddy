package com.tech.buddy.service;

import com.tech.buddy.dto.OrderDetailDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface OrderDetailService {
    OrderDetailDTO save(OrderDetailDTO dto);

    void save(List<OrderDetailDTO> dtos);

    void deleteById(Integer id);

    Optional<OrderDetailDTO> findById(Integer id);

    List<OrderDetailDTO> findAll();

    Page<OrderDetailDTO> findAll(Pageable pageable);

    OrderDetailDTO updateById(OrderDetailDTO dto);
}