package com.tech.buddy.service;

import com.tech.buddy.dto.CartItemDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CartItemService {
    CartItemDTO save(CartItemDTO dto);

    void save(List<CartItemDTO> dtos);

    void deleteById(Integer id);

    Optional<CartItemDTO> findById(Integer id);

    List<CartItemDTO> findAll();

    Page<CartItemDTO> findAll(Pageable pageable);

    CartItemDTO updateById(CartItemDTO dto);
}