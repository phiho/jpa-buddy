package com.tech.buddy.service;

import com.tech.buddy.dto.ProductImageDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ProductImageService {
    ProductImageDTO save(ProductImageDTO dto);

    void save(List<ProductImageDTO> dtos);

    void deleteById(Integer id);

    Optional<ProductImageDTO> findById(Integer id);

    List<ProductImageDTO> findAll();

    Page<ProductImageDTO> findAll(Pageable pageable);

    ProductImageDTO updateById(ProductImageDTO dto);
}