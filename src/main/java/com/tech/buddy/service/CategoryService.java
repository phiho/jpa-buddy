package com.tech.buddy.service;

import com.tech.buddy.dto.CategoryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    CategoryDTO save(CategoryDTO dto);

    void save(List<CategoryDTO> dtos);

    void deleteById(Integer id);

    Optional<CategoryDTO> findById(Integer id);

    List<CategoryDTO> findAll();

    Page<CategoryDTO> findAll(Pageable pageable);

    CategoryDTO updateById(CategoryDTO dto);
}