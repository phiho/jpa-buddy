package com.tech.buddy.service;

import com.tech.buddy.dto.CurrencyDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CurrencyService {
    CurrencyDTO save(CurrencyDTO dto);

    void save(List<CurrencyDTO> dtos);

    void deleteById(Integer id);

    Optional<CurrencyDTO> findById(Integer id);

    List<CurrencyDTO> findAll();

    Page<CurrencyDTO> findAll(Pageable pageable);

    CurrencyDTO updateById(CurrencyDTO dto);
}