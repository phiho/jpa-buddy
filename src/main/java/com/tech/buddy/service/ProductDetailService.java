package com.tech.buddy.service;

import com.tech.buddy.dto.ProductDetailDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ProductDetailService {
    ProductDetailDTO save(ProductDetailDTO dto);

    void save(List<ProductDetailDTO> dtos);

    void deleteById(Integer id);

    Optional<ProductDetailDTO> findById(Integer id);

    List<ProductDetailDTO> findAll();

    Page<ProductDetailDTO> findAll(Pageable pageable);

    ProductDetailDTO updateById(ProductDetailDTO dto);
}