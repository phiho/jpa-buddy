package com.tech.buddy.service;

import com.tech.buddy.dto.ReviewDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ReviewService {
    ReviewDTO save(ReviewDTO dto);

    void save(List<ReviewDTO> dtos);

    void deleteById(Integer id);

    Optional<ReviewDTO> findById(Integer id);

    List<ReviewDTO> findAll();

    Page<ReviewDTO> findAll(Pageable pageable);

    ReviewDTO updateById(ReviewDTO dto);
}