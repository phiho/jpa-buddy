package com.tech.buddy.service;

import com.tech.buddy.dto.OrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface OrderService {
    OrderDTO save(OrderDTO dto);

    void save(List<OrderDTO> dtos);

    void deleteById(Integer id);

    Optional<OrderDTO> findById(Integer id);

    List<OrderDTO> findAll();

    Page<OrderDTO> findAll(Pageable pageable);

    OrderDTO updateById(OrderDTO dto);
}