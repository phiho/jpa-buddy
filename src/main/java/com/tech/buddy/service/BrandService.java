package com.tech.buddy.service;

import com.tech.buddy.dto.BrandDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface BrandService {
    BrandDTO save(BrandDTO dto);

    void save(List<BrandDTO> dtos);

    void deleteById(Integer id);

    Optional<BrandDTO> findById(Integer id);

    List<BrandDTO> findAll();

    Page<BrandDTO> findAll(Pageable pageable);

    BrandDTO updateById(BrandDTO dto);
}