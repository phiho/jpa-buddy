package com.tech.buddy.service;

import com.tech.buddy.dto.CountryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CountryService {
    CountryDTO save(CountryDTO dto);

    void save(List<CountryDTO> dtos);

    void deleteById(Integer id);

    Optional<CountryDTO> findById(Integer id);

    List<CountryDTO> findAll();

    Page<CountryDTO> findAll(Pageable pageable);

    CountryDTO updateById(CountryDTO dto);
}