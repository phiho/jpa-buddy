package com.tech.buddy.service.impl;

import com.tech.buddy.dao.ProductDetailRepository;
import com.tech.buddy.domain.ProductDetail;
import com.tech.buddy.dto.ProductDetailDTO;
import com.tech.buddy.mapper.ProductDetailMapper;
import com.tech.buddy.service.ProductDetailService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductDetailServiceImpl implements ProductDetailService {
    private ProductDetailMapper mapper = Mappers.getMapper(ProductDetailMapper.class);
    private final ProductDetailRepository repository;

    public ProductDetailServiceImpl(ProductDetailRepository repository) {
        this.repository = repository;
    }

    @Override
    public ProductDetailDTO save(ProductDetailDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<ProductDetailDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<ProductDetailDTO> findById(Integer id) {
        Optional<ProductDetail> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<ProductDetailDTO> findAll() {
        return mapper.toDtoList((List<ProductDetail>) repository.findAll());
    }

    @Override
    public Page<ProductDetailDTO> findAll(Pageable pageable) {
        Page<ProductDetail> entityPage = repository.findAll(pageable);
        List<ProductDetailDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public ProductDetailDTO updateById(ProductDetailDTO dto) {
        Optional<ProductDetailDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}