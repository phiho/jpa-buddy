package com.tech.buddy.service.impl;

import com.tech.buddy.dao.BrandRepository;
import com.tech.buddy.domain.Brand;
import com.tech.buddy.dto.BrandDTO;
import com.tech.buddy.mapper.BrandMapper;
import com.tech.buddy.service.BrandService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BrandServiceImpl implements BrandService {
    private BrandMapper mapper = Mappers.getMapper(BrandMapper.class);
    private final BrandRepository repository;

    public BrandServiceImpl(BrandRepository repository) {
        this.repository = repository;
    }

    @Override
    public BrandDTO save(BrandDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<BrandDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<BrandDTO> findById(Integer id) {
        Optional<Brand> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<BrandDTO> findAll() {
        return mapper.toDtoList((List<Brand>) repository.findAll());
    }

    @Override
    public Page<BrandDTO> findAll(Pageable pageable) {
        Page<Brand> entityPage = repository.findAll(pageable);
        List<BrandDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public BrandDTO updateById(BrandDTO dto) {
        Optional<BrandDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}