package com.tech.buddy.service.impl;

import com.tech.buddy.dao.OrderTrackRepository;
import com.tech.buddy.domain.OrderTrack;
import com.tech.buddy.dto.OrderTrackDTO;
import com.tech.buddy.mapper.OrderTrackMapper;
import com.tech.buddy.service.OrderTrackService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class OrderTrackServiceImpl implements OrderTrackService {
    private OrderTrackMapper mapper = Mappers.getMapper(OrderTrackMapper.class);
    private final OrderTrackRepository repository;

    public OrderTrackServiceImpl(OrderTrackRepository repository) {
        this.repository = repository;
    }

    @Override
    public OrderTrackDTO save(OrderTrackDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<OrderTrackDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<OrderTrackDTO> findById(Integer id) {
        Optional<OrderTrack> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<OrderTrackDTO> findAll() {
        return mapper.toDtoList((List<OrderTrack>) repository.findAll());
    }

    @Override
    public Page<OrderTrackDTO> findAll(Pageable pageable) {
        Page<OrderTrack> entityPage = repository.findAll(pageable);
        List<OrderTrackDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public OrderTrackDTO updateById(OrderTrackDTO dto) {
        Optional<OrderTrackDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}