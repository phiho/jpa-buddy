package com.tech.buddy.service.impl;

import com.tech.buddy.dao.ShippingRateRepository;
import com.tech.buddy.domain.ShippingRate;
import com.tech.buddy.dto.ShippingRateDTO;
import com.tech.buddy.mapper.ShippingRateMapper;
import com.tech.buddy.service.ShippingRateService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ShippingRateServiceImpl implements ShippingRateService {
    private ShippingRateMapper mapper = Mappers.getMapper(ShippingRateMapper.class);
    private final ShippingRateRepository repository;

    public ShippingRateServiceImpl(ShippingRateRepository repository) {
        this.repository = repository;
    }

    @Override
    public ShippingRateDTO save(ShippingRateDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<ShippingRateDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<ShippingRateDTO> findById(Integer id) {
        Optional<ShippingRate> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<ShippingRateDTO> findAll() {
        return mapper.toDtoList((List<ShippingRate>) repository.findAll());
    }

    @Override
    public Page<ShippingRateDTO> findAll(Pageable pageable) {
        Page<ShippingRate> entityPage = repository.findAll(pageable);
        List<ShippingRateDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public ShippingRateDTO updateById(ShippingRateDTO dto) {
        Optional<ShippingRateDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}