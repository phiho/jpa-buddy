package com.tech.buddy.service.impl;

import com.tech.buddy.dao.BrandsCategoryRepository;
import com.tech.buddy.domain.BrandsCategory;
import com.tech.buddy.domain.BrandsCategoryId;
import com.tech.buddy.dto.BrandsCategoryDTO;
import com.tech.buddy.mapper.BrandsCategoryMapper;
import com.tech.buddy.service.BrandsCategoryService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class BrandsCategoryServiceImpl implements BrandsCategoryService {
    private BrandsCategoryMapper mapper = Mappers.getMapper(BrandsCategoryMapper.class);
    private final BrandsCategoryRepository repository;

    public BrandsCategoryServiceImpl(BrandsCategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public BrandsCategoryDTO save(BrandsCategoryDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<BrandsCategoryDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(BrandsCategoryId id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<BrandsCategoryDTO> findById(BrandsCategoryId id) {
        Optional<BrandsCategory> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<BrandsCategoryDTO> findAll() {
        return mapper.toDtoList((List<BrandsCategory>) repository.findAll());
    }

    @Override
    public Page<BrandsCategoryDTO> findAll(Pageable pageable) {
        Page<BrandsCategory> entityPage = repository.findAll(pageable);
        List<BrandsCategoryDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public BrandsCategoryDTO updateById(BrandsCategoryDTO dto) {
        Optional<BrandsCategoryDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}