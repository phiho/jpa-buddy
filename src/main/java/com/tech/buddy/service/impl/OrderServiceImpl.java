package com.tech.buddy.service.impl;

import com.tech.buddy.dao.OrderRepository;
import com.tech.buddy.domain.Order;
import com.tech.buddy.dto.OrderDTO;
import com.tech.buddy.mapper.OrderMapper;
import com.tech.buddy.service.OrderService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {
    private OrderMapper mapper = Mappers.getMapper(OrderMapper.class);
    private final OrderRepository repository;

    public OrderServiceImpl(OrderRepository repository) {
        this.repository = repository;
    }

    @Override
    public OrderDTO save(OrderDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<OrderDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<OrderDTO> findById(Integer id) {
        Optional<Order> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<OrderDTO> findAll() {
        return mapper.toDtoList((List<Order>) repository.findAll());
    }

    @Override
    public Page<OrderDTO> findAll(Pageable pageable) {
        Page<Order> entityPage = repository.findAll(pageable);
        List<OrderDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public OrderDTO updateById(OrderDTO dto) {
        Optional<OrderDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}