package com.tech.buddy.service.impl;

import com.tech.buddy.dao.ReviewRepository;
import com.tech.buddy.domain.Review;
import com.tech.buddy.dto.ReviewDTO;
import com.tech.buddy.mapper.ReviewMapper;
import com.tech.buddy.service.ReviewService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ReviewServiceImpl implements ReviewService {
    private ReviewMapper mapper = Mappers.getMapper(ReviewMapper.class);
    private final ReviewRepository repository;

    public ReviewServiceImpl(ReviewRepository repository) {
        this.repository = repository;
    }

    @Override
    public ReviewDTO save(ReviewDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<ReviewDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<ReviewDTO> findById(Integer id) {
        Optional<Review> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<ReviewDTO> findAll() {
        return mapper.toDtoList((List<Review>) repository.findAll());
    }

    @Override
    public Page<ReviewDTO> findAll(Pageable pageable) {
        Page<Review> entityPage = repository.findAll(pageable);
        List<ReviewDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public ReviewDTO updateById(ReviewDTO dto) {
        Optional<ReviewDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}