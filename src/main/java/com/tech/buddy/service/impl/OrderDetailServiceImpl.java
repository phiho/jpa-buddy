package com.tech.buddy.service.impl;

import com.tech.buddy.dao.OrderDetailRepository;
import com.tech.buddy.domain.OrderDetail;
import com.tech.buddy.dto.OrderDetailDTO;
import com.tech.buddy.mapper.OrderDetailMapper;
import com.tech.buddy.service.OrderDetailService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class OrderDetailServiceImpl implements OrderDetailService {
    private OrderDetailMapper mapper = Mappers.getMapper(OrderDetailMapper.class);
    private final OrderDetailRepository repository;

    public OrderDetailServiceImpl(OrderDetailRepository repository) {
        this.repository = repository;
    }

    @Override
    public OrderDetailDTO save(OrderDetailDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<OrderDetailDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<OrderDetailDTO> findById(Integer id) {
        Optional<OrderDetail> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<OrderDetailDTO> findAll() {
        return mapper.toDtoList((List<OrderDetail>) repository.findAll());
    }

    @Override
    public Page<OrderDetailDTO> findAll(Pageable pageable) {
        Page<OrderDetail> entityPage = repository.findAll(pageable);
        List<OrderDetailDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public OrderDetailDTO updateById(OrderDetailDTO dto) {
        Optional<OrderDetailDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}