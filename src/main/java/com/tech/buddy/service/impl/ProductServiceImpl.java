package com.tech.buddy.service.impl;

import com.tech.buddy.dao.ProductRepository;
import com.tech.buddy.domain.Product;
import com.tech.buddy.dto.ProductDTO;
import com.tech.buddy.mapper.ProductMapper;
import com.tech.buddy.service.ProductService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductServiceImpl implements ProductService {
    private ProductMapper mapper = Mappers.getMapper(ProductMapper.class);
    private final ProductRepository repository;

    public ProductServiceImpl(ProductRepository repository) {
        this.repository = repository;
    }

    @Override
    public ProductDTO save(ProductDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<ProductDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<ProductDTO> findById(Integer id) {
        Optional<Product> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<ProductDTO> findAll() {
        return mapper.toDtoList((List<Product>) repository.findAll());
    }

    @Override
    public Page<ProductDTO> findAll(Pageable pageable) {
        Page<Product> entityPage = repository.findAll(pageable);
        List<ProductDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public ProductDTO updateById(ProductDTO dto) {
        Optional<ProductDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}