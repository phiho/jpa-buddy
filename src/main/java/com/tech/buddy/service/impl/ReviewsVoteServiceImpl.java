package com.tech.buddy.service.impl;

import com.tech.buddy.dao.ReviewsVoteRepository;
import com.tech.buddy.domain.ReviewsVote;
import com.tech.buddy.dto.ReviewsVoteDTO;
import com.tech.buddy.mapper.ReviewsVoteMapper;
import com.tech.buddy.service.ReviewsVoteService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ReviewsVoteServiceImpl implements ReviewsVoteService {
    private ReviewsVoteMapper mapper = Mappers.getMapper(ReviewsVoteMapper.class);
    private final ReviewsVoteRepository repository;

    public ReviewsVoteServiceImpl(ReviewsVoteRepository repository) {
        this.repository = repository;
    }

    @Override
    public ReviewsVoteDTO save(ReviewsVoteDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<ReviewsVoteDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<ReviewsVoteDTO> findById(Integer id) {
        Optional<ReviewsVote> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<ReviewsVoteDTO> findAll() {
        return mapper.toDtoList((List<ReviewsVote>) repository.findAll());
    }

    @Override
    public Page<ReviewsVoteDTO> findAll(Pageable pageable) {
        Page<ReviewsVote> entityPage = repository.findAll(pageable);
        List<ReviewsVoteDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public ReviewsVoteDTO updateById(ReviewsVoteDTO dto) {
        Optional<ReviewsVoteDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}