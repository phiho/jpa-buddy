package com.tech.buddy.service.impl;

import com.tech.buddy.dao.CartItemRepository;
import com.tech.buddy.domain.CartItem;
import com.tech.buddy.dto.CartItemDTO;
import com.tech.buddy.mapper.CartItemMapper;
import com.tech.buddy.service.CartItemService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CartItemServiceImpl implements CartItemService {
    private CartItemMapper mapper = Mappers.getMapper(CartItemMapper.class);
    private final CartItemRepository repository;

    public CartItemServiceImpl(CartItemRepository repository) {
        this.repository = repository;
    }

    @Override
    public CartItemDTO save(CartItemDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<CartItemDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<CartItemDTO> findById(Integer id) {
        Optional<CartItem> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<CartItemDTO> findAll() {
        return mapper.toDtoList((List<CartItem>) repository.findAll());
    }

    @Override
    public Page<CartItemDTO> findAll(Pageable pageable) {
        Page<CartItem> entityPage = repository.findAll(pageable);
        List<CartItemDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public CartItemDTO updateById(CartItemDTO dto) {
        Optional<CartItemDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}