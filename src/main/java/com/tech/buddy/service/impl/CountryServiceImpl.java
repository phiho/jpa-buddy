package com.tech.buddy.service.impl;

import com.tech.buddy.dao.CountryRepository;
import com.tech.buddy.domain.Country;
import com.tech.buddy.dto.CountryDTO;
import com.tech.buddy.mapper.CountryMapper;
import com.tech.buddy.service.CountryService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CountryServiceImpl implements CountryService {
    private CountryMapper mapper = Mappers.getMapper(CountryMapper.class);
    private final CountryRepository repository;

    public CountryServiceImpl(CountryRepository repository) {
        this.repository = repository;
    }

    @Override
    public CountryDTO save(CountryDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<CountryDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<CountryDTO> findById(Integer id) {
        Optional<Country> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<CountryDTO> findAll() {
        return mapper.toDtoList((List<Country>) repository.findAll());
    }

    @Override
    public Page<CountryDTO> findAll(Pageable pageable) {
        Page<Country> entityPage = repository.findAll(pageable);
        List<CountryDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public CountryDTO updateById(CountryDTO dto) {
        Optional<CountryDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}