package com.tech.buddy.service.impl;

import com.tech.buddy.dao.ProductImageRepository;
import com.tech.buddy.domain.ProductImage;
import com.tech.buddy.dto.ProductImageDTO;
import com.tech.buddy.mapper.ProductImageMapper;
import com.tech.buddy.service.ProductImageService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProductImageServiceImpl implements ProductImageService {
    private ProductImageMapper mapper = Mappers.getMapper(ProductImageMapper.class);
    private final ProductImageRepository repository;

    public ProductImageServiceImpl(ProductImageRepository repository) {
        this.repository = repository;
    }

    @Override
    public ProductImageDTO save(ProductImageDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<ProductImageDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<ProductImageDTO> findById(Integer id) {
        Optional<ProductImage> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<ProductImageDTO> findAll() {
        return mapper.toDtoList((List<ProductImage>) repository.findAll());
    }

    @Override
    public Page<ProductImageDTO> findAll(Pageable pageable) {
        Page<ProductImage> entityPage = repository.findAll(pageable);
        List<ProductImageDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public ProductImageDTO updateById(ProductImageDTO dto) {
        Optional<ProductImageDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}