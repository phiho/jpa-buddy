package com.tech.buddy.service.impl;

import com.tech.buddy.dao.CustomerRepository;
import com.tech.buddy.domain.Customer;
import com.tech.buddy.dto.CustomerDTO;
import com.tech.buddy.mapper.CustomerMapper;
import com.tech.buddy.service.CustomerService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
    private CustomerMapper mapper = Mappers.getMapper(CustomerMapper.class);
    private final CustomerRepository repository;

    public CustomerServiceImpl(CustomerRepository repository) {
        this.repository = repository;
    }

    @Override
    public CustomerDTO save(CustomerDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<CustomerDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<CustomerDTO> findById(Integer id) {
        Optional<Customer> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<CustomerDTO> findAll() {
        return mapper.toDtoList((List<Customer>) repository.findAll());
    }

    @Override
    public Page<CustomerDTO> findAll(Pageable pageable) {
        Page<Customer> entityPage = repository.findAll(pageable);
        List<CustomerDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public CustomerDTO updateById(CustomerDTO dto) {
        Optional<CustomerDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}