package com.tech.buddy.service.impl;

import com.tech.buddy.dao.StateRepository;
import com.tech.buddy.domain.State;
import com.tech.buddy.dto.StateDTO;
import com.tech.buddy.mapper.StateMapper;
import com.tech.buddy.service.StateService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class StateServiceImpl implements StateService {
    private StateMapper mapper = Mappers.getMapper(StateMapper.class);
    private final StateRepository repository;

    public StateServiceImpl(StateRepository repository) {
        this.repository = repository;
    }

    @Override
    public StateDTO save(StateDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<StateDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<StateDTO> findById(Integer id) {
        Optional<State> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<StateDTO> findAll() {
        return mapper.toDtoList((List<State>) repository.findAll());
    }

    @Override
    public Page<StateDTO> findAll(Pageable pageable) {
        Page<State> entityPage = repository.findAll(pageable);
        List<StateDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public StateDTO updateById(StateDTO dto) {
        Optional<StateDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}