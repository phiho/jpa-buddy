package com.tech.buddy.service.impl;

import com.tech.buddy.dao.SettingRepository;
import com.tech.buddy.domain.Setting;
import com.tech.buddy.dto.SettingDTO;
import com.tech.buddy.mapper.SettingMapper;
import com.tech.buddy.service.SettingService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class SettingServiceImpl implements SettingService {
    private SettingMapper mapper = Mappers.getMapper(SettingMapper.class);
    private final SettingRepository repository;

    public SettingServiceImpl(SettingRepository repository) {
        this.repository = repository;
    }

    @Override
    public SettingDTO save(SettingDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<SettingDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(String id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<SettingDTO> findById(String id) {
        Optional<Setting> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<SettingDTO> findAll() {
        return mapper.toDtoList((List<Setting>) repository.findAll());
    }

    @Override
    public Page<SettingDTO> findAll(Pageable pageable) {
        Page<Setting> entityPage = repository.findAll(pageable);
        List<SettingDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public SettingDTO updateById(SettingDTO dto) {
        Optional<SettingDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}