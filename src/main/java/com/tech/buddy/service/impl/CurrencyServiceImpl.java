package com.tech.buddy.service.impl;

import com.tech.buddy.dao.CurrencyRepository;
import com.tech.buddy.domain.Currency;
import com.tech.buddy.dto.CurrencyDTO;
import com.tech.buddy.mapper.CurrencyMapper;
import com.tech.buddy.service.CurrencyService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CurrencyServiceImpl implements CurrencyService {
    private CurrencyMapper mapper = Mappers.getMapper(CurrencyMapper.class);
    private final CurrencyRepository repository;

    public CurrencyServiceImpl(CurrencyRepository repository) {
        this.repository = repository;
    }

    @Override
    public CurrencyDTO save(CurrencyDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<CurrencyDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<CurrencyDTO> findById(Integer id) {
        Optional<Currency> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<CurrencyDTO> findAll() {
        return mapper.toDtoList((List<Currency>) repository.findAll());
    }

    @Override
    public Page<CurrencyDTO> findAll(Pageable pageable) {
        Page<Currency> entityPage = repository.findAll(pageable);
        List<CurrencyDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public CurrencyDTO updateById(CurrencyDTO dto) {
        Optional<CurrencyDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}