package com.tech.buddy.service.impl;

import com.tech.buddy.dao.CategoryRepository;
import com.tech.buddy.domain.Category;
import com.tech.buddy.dto.CategoryDTO;
import com.tech.buddy.mapper.CategoryMapper;
import com.tech.buddy.service.CategoryService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {
    private CategoryMapper mapper = Mappers.getMapper(CategoryMapper.class);
    private final CategoryRepository repository;

    public CategoryServiceImpl(CategoryRepository repository) {
        this.repository = repository;
    }

    @Override
    public CategoryDTO save(CategoryDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<CategoryDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<CategoryDTO> findById(Integer id) {
        Optional<Category> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<CategoryDTO> findAll() {
        return mapper.toDtoList((List<Category>) repository.findAll());
    }

    @Override
    public Page<CategoryDTO> findAll(Pageable pageable) {
        Page<Category> entityPage = repository.findAll(pageable);
        List<CategoryDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public CategoryDTO updateById(CategoryDTO dto) {
        Optional<CategoryDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}