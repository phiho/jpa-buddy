package com.tech.buddy.service.impl;

import com.tech.buddy.dao.RoleRepository;
import com.tech.buddy.domain.Role;
import com.tech.buddy.dto.RoleDTO;
import com.tech.buddy.mapper.RoleMapper;
import com.tech.buddy.service.RoleService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {
    private RoleMapper mapper = Mappers.getMapper(RoleMapper.class);
    private final RoleRepository repository;

    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public RoleDTO save(RoleDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<RoleDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<RoleDTO> findById(Integer id) {
        Optional<Role> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<RoleDTO> findAll() {
        return mapper.toDtoList((List<Role>) repository.findAll());
    }

    @Override
    public Page<RoleDTO> findAll(Pageable pageable) {
        Page<Role> entityPage = repository.findAll(pageable);
        List<RoleDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public RoleDTO updateById(RoleDTO dto) {
        Optional<RoleDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}