package com.tech.buddy.service.impl;

import com.tech.buddy.dao.AddressRepository;
import com.tech.buddy.domain.Address;
import com.tech.buddy.dto.AddressDTO;
import com.tech.buddy.mapper.AddressMapper;
import com.tech.buddy.service.AddressService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AddressServiceImpl implements AddressService {
    private AddressMapper mapper = Mappers.getMapper(AddressMapper.class);
    private final AddressRepository repository;

    public AddressServiceImpl(AddressRepository repository) {
        this.repository = repository;
    }

    @Override
    public AddressDTO save(AddressDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<AddressDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<AddressDTO> findById(Integer id) {
        Optional<Address> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<AddressDTO> findAll() {
        return mapper.toDtoList((List<Address>) repository.findAll());
    }

    @Override
    public Page<AddressDTO> findAll(Pageable pageable) {
        Page<Address> entityPage = repository.findAll(pageable);
        List<AddressDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public AddressDTO updateById(AddressDTO dto) {
        Optional<AddressDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}