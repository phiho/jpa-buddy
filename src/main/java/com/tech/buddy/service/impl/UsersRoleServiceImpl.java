package com.tech.buddy.service.impl;

import com.tech.buddy.dao.UsersRoleRepository;
import com.tech.buddy.domain.UsersRole;
import com.tech.buddy.domain.UsersRoleId;
import com.tech.buddy.dto.UsersRoleDTO;
import com.tech.buddy.mapper.UsersRoleMapper;
import com.tech.buddy.service.UsersRoleService;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UsersRoleServiceImpl implements UsersRoleService {
    private UsersRoleMapper mapper = Mappers.getMapper(UsersRoleMapper.class);
    private final UsersRoleRepository repository;

    public UsersRoleServiceImpl(UsersRoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public UsersRoleDTO save(UsersRoleDTO dto) {
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    @Override
    public void save(List<UsersRoleDTO> dtos) {
        repository.saveAll(mapper.toEntityList(dtos));
    }

    @Override
    public void deleteById(UsersRoleId id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<UsersRoleDTO> findById(UsersRoleId id) {
        Optional<UsersRole> entityOptional = repository.findById(id);
        return entityOptional.map(entity -> Optional.ofNullable(mapper.toDto(entity))).orElse(null);
    }

    @Override
    public List<UsersRoleDTO> findAll() {
        return mapper.toDtoList((List<UsersRole>) repository.findAll());
    }

    @Override
    public Page<UsersRoleDTO> findAll(Pageable pageable) {
        Page<UsersRole> entityPage = repository.findAll(pageable);
        List<UsersRoleDTO> dtos = mapper.toDtoList(entityPage.getContent());
        return new PageImpl<>(dtos, pageable, entityPage.getTotalElements());
    }

    @Override
    public UsersRoleDTO updateById(UsersRoleDTO dto) {
        Optional<UsersRoleDTO> optionalDto = findById(dto.getId());
        if (optionalDto.isPresent()) {
            return save(dto);
        }
        return null;
    }
}