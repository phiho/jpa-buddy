package com.tech.buddy.service;

import com.tech.buddy.dto.ProductDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    ProductDTO save(ProductDTO dto);

    void save(List<ProductDTO> dtos);

    void deleteById(Integer id);

    Optional<ProductDTO> findById(Integer id);

    List<ProductDTO> findAll();

    Page<ProductDTO> findAll(Pageable pageable);

    ProductDTO updateById(ProductDTO dto);
}