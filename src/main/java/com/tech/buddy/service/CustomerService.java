package com.tech.buddy.service;

import com.tech.buddy.dto.CustomerDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    CustomerDTO save(CustomerDTO dto);

    void save(List<CustomerDTO> dtos);

    void deleteById(Integer id);

    Optional<CustomerDTO> findById(Integer id);

    List<CustomerDTO> findAll();

    Page<CustomerDTO> findAll(Pageable pageable);

    CustomerDTO updateById(CustomerDTO dto);
}