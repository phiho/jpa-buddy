package com.tech.buddy.service;

import com.tech.buddy.dto.ShippingRateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ShippingRateService {
    ShippingRateDTO save(ShippingRateDTO dto);

    void save(List<ShippingRateDTO> dtos);

    void deleteById(Integer id);

    Optional<ShippingRateDTO> findById(Integer id);

    List<ShippingRateDTO> findAll();

    Page<ShippingRateDTO> findAll(Pageable pageable);

    ShippingRateDTO updateById(ShippingRateDTO dto);
}