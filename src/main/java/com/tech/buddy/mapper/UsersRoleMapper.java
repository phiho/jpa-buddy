package com.tech.buddy.mapper;

import com.tech.buddy.domain.UsersRole;
import com.tech.buddy.dto.UsersRoleDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UsersRoleMapper extends EntityMapper<UsersRoleDTO, UsersRole> {
}