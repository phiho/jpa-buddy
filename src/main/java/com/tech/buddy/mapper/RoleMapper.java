package com.tech.buddy.mapper;

import com.tech.buddy.domain.Role;
import com.tech.buddy.dto.RoleDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleMapper extends EntityMapper<RoleDTO, Role> {
}