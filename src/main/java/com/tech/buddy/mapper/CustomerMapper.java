package com.tech.buddy.mapper;

import com.tech.buddy.domain.Customer;
import com.tech.buddy.dto.CustomerDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper extends EntityMapper<CustomerDTO, Customer> {
}