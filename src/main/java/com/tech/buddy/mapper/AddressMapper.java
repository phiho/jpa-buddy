package com.tech.buddy.mapper;

import com.tech.buddy.domain.Address;
import com.tech.buddy.dto.AddressDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper extends EntityMapper<AddressDTO, Address> {
}