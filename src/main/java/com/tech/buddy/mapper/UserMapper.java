package com.tech.buddy.mapper;

import com.tech.buddy.domain.User;
import com.tech.buddy.dto.UserDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper extends EntityMapper<UserDTO, User> {
}