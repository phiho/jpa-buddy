package com.tech.buddy.mapper;

import com.tech.buddy.domain.State;
import com.tech.buddy.dto.StateDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StateMapper extends EntityMapper<StateDTO, State> {
}