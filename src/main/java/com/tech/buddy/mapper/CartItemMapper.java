package com.tech.buddy.mapper;

import com.tech.buddy.domain.CartItem;
import com.tech.buddy.dto.CartItemDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CartItemMapper extends EntityMapper<CartItemDTO, CartItem> {
}