package com.tech.buddy.mapper;

import com.tech.buddy.domain.BrandsCategory;
import com.tech.buddy.dto.BrandsCategoryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BrandsCategoryMapper extends EntityMapper<BrandsCategoryDTO, BrandsCategory> {
}