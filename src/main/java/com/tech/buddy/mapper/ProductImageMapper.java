package com.tech.buddy.mapper;

import com.tech.buddy.domain.ProductImage;
import com.tech.buddy.dto.ProductImageDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductImageMapper extends EntityMapper<ProductImageDTO, ProductImage> {
}