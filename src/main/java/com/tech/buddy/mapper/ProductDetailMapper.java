package com.tech.buddy.mapper;

import com.tech.buddy.domain.ProductDetail;
import com.tech.buddy.dto.ProductDetailDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductDetailMapper extends EntityMapper<ProductDetailDTO, ProductDetail> {
}