package com.tech.buddy.mapper;

import com.tech.buddy.domain.Product;
import com.tech.buddy.dto.ProductDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper extends EntityMapper<ProductDTO, Product> {
}