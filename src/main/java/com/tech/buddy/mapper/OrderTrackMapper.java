package com.tech.buddy.mapper;

import com.tech.buddy.domain.OrderTrack;
import com.tech.buddy.dto.OrderTrackDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderTrackMapper extends EntityMapper<OrderTrackDTO, OrderTrack> {
}