package com.tech.buddy.mapper;

import com.tech.buddy.domain.Country;
import com.tech.buddy.dto.CountryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CountryMapper extends EntityMapper<CountryDTO, Country> {
}