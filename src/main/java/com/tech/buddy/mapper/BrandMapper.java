package com.tech.buddy.mapper;

import com.tech.buddy.domain.Brand;
import com.tech.buddy.dto.BrandDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BrandMapper extends EntityMapper<BrandDTO, Brand> {
}