package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.Review;
import com.tech.buddy.dto.ReviewDTO;
import com.tech.buddy.mapper.ReviewMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class ReviewMapperImpl implements ReviewMapper {
    @Override
    public Review toEntity(ReviewDTO dto) {
        Review entity = new Review();
        entity.setId(dto.getId());
        entity.setComment(dto.getComment());
        entity.setHeadline(dto.getHeadline());
        entity.setRating(dto.getRating());
        entity.setReviewTime(dto.getReviewTime());
        entity.setVotes(dto.getVotes());
        entity.setCustomer(dto.getCustomer());
        entity.setProduct(dto.getProduct());
        return entity;
    }

    @Override
    public ReviewDTO toDto(Review entity) {
        ReviewDTO dto = new ReviewDTO();
        dto.setId(entity.getId());
        dto.setComment(entity.getComment());
        dto.setHeadline(entity.getHeadline());
        dto.setRating(entity.getRating());
        dto.setReviewTime(entity.getReviewTime());
        dto.setVotes(entity.getVotes());
        dto.setCustomer(entity.getCustomer());
        dto.setProduct(entity.getProduct());
        return dto;
    }

    @Override
    public List<Review> toEntityList(List<ReviewDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<ReviewDTO> toDtoList(List<Review> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}