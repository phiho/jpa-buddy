package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.CartItem;
import com.tech.buddy.dto.CartItemDTO;
import com.tech.buddy.mapper.CartItemMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class CartItemMapperImpl implements CartItemMapper {
    @Override
    public CartItem toEntity(CartItemDTO dto) {
        CartItem entity = new CartItem();
        entity.setId(dto.getId());
        entity.setQuantity(dto.getQuantity());
        entity.setCustomer(dto.getCustomer());
        entity.setProduct(dto.getProduct());
        return entity;
    }

    @Override
    public CartItemDTO toDto(CartItem entity) {
        CartItemDTO dto = new CartItemDTO();
        dto.setId(entity.getId());
        dto.setQuantity(entity.getQuantity());
        dto.setCustomer(entity.getCustomer());
        dto.setProduct(entity.getProduct());
        return dto;
    }

    @Override
    public List<CartItem> toEntityList(List<CartItemDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<CartItemDTO> toDtoList(List<CartItem> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}