package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.Setting;
import com.tech.buddy.dto.SettingDTO;
import com.tech.buddy.mapper.SettingMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class SettingMapperImpl implements SettingMapper {
    @Override
    public Setting toEntity(SettingDTO dto) {
        Setting entity = new Setting();
        entity.setId(dto.getId());
        entity.setCategory(dto.getCategory());
        entity.setValue(dto.getValue());
        return entity;
    }

    @Override
    public SettingDTO toDto(Setting entity) {
        SettingDTO dto = new SettingDTO();
        dto.setId(entity.getId());
        dto.setCategory(entity.getCategory());
        dto.setValue(entity.getValue());
        return dto;
    }

    @Override
    public List<Setting> toEntityList(List<SettingDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<SettingDTO> toDtoList(List<Setting> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}