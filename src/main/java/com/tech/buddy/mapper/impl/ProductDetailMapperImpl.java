package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.ProductDetail;
import com.tech.buddy.dto.ProductDetailDTO;
import com.tech.buddy.mapper.ProductDetailMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class ProductDetailMapperImpl implements ProductDetailMapper {
    @Override
    public ProductDetail toEntity(ProductDetailDTO dto) {
        ProductDetail entity = new ProductDetail();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setValue(dto.getValue());
        entity.setProduct(dto.getProduct());
        return entity;
    }

    @Override
    public ProductDetailDTO toDto(ProductDetail entity) {
        ProductDetailDTO dto = new ProductDetailDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setValue(entity.getValue());
        dto.setProduct(entity.getProduct());
        return dto;
    }

    @Override
    public List<ProductDetail> toEntityList(List<ProductDetailDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<ProductDetailDTO> toDtoList(List<ProductDetail> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}