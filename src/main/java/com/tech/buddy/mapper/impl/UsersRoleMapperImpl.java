package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.UsersRole;
import com.tech.buddy.dto.UsersRoleDTO;
import com.tech.buddy.mapper.UsersRoleMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class UsersRoleMapperImpl implements UsersRoleMapper {
    @Override
    public UsersRole toEntity(UsersRoleDTO dto) {
        UsersRole entity = new UsersRole();
        entity.setId(dto.getId());
        entity.setUser(dto.getUser());
        entity.setRole(dto.getRole());
        return entity;
    }

    @Override
    public UsersRoleDTO toDto(UsersRole entity) {
        UsersRoleDTO dto = new UsersRoleDTO();
        dto.setId(entity.getId());
        dto.setUser(entity.getUser());
        dto.setRole(entity.getRole());
        return dto;
    }

    @Override
    public List<UsersRole> toEntityList(List<UsersRoleDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<UsersRoleDTO> toDtoList(List<UsersRole> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}