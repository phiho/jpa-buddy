package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.OrderTrack;
import com.tech.buddy.dto.OrderTrackDTO;
import com.tech.buddy.mapper.OrderTrackMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class OrderTrackMapperImpl implements OrderTrackMapper {
    @Override
    public OrderTrack toEntity(OrderTrackDTO dto) {
        OrderTrack entity = new OrderTrack();
        entity.setId(dto.getId());
        entity.setNotes(dto.getNotes());
        entity.setStatus(dto.getStatus());
        entity.setUpdatedTime(dto.getUpdatedTime());
        entity.setOrder(dto.getOrder());
        return entity;
    }

    @Override
    public OrderTrackDTO toDto(OrderTrack entity) {
        OrderTrackDTO dto = new OrderTrackDTO();
        dto.setId(entity.getId());
        dto.setNotes(entity.getNotes());
        dto.setStatus(entity.getStatus());
        dto.setUpdatedTime(entity.getUpdatedTime());
        dto.setOrder(entity.getOrder());
        return dto;
    }

    @Override
    public List<OrderTrack> toEntityList(List<OrderTrackDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<OrderTrackDTO> toDtoList(List<OrderTrack> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}