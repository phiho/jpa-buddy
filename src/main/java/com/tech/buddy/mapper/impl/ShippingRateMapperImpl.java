package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.ShippingRate;
import com.tech.buddy.dto.ShippingRateDTO;
import com.tech.buddy.mapper.ShippingRateMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class ShippingRateMapperImpl implements ShippingRateMapper {
    @Override
    public ShippingRate toEntity(ShippingRateDTO dto) {
        ShippingRate entity = new ShippingRate();
        entity.setId(dto.getId());
        entity.setCodSupported(dto.getCodSupported());
        entity.setDays(dto.getDays());
        entity.setRate(dto.getRate());
        entity.setState(dto.getState());
        entity.setCountry(dto.getCountry());
        return entity;
    }

    @Override
    public ShippingRateDTO toDto(ShippingRate entity) {
        ShippingRateDTO dto = new ShippingRateDTO();
        dto.setId(entity.getId());
        dto.setCodSupported(entity.getCodSupported());
        dto.setDays(entity.getDays());
        dto.setRate(entity.getRate());
        dto.setState(entity.getState());
        dto.setCountry(entity.getCountry());
        return dto;
    }

    @Override
    public List<ShippingRate> toEntityList(List<ShippingRateDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<ShippingRateDTO> toDtoList(List<ShippingRate> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}