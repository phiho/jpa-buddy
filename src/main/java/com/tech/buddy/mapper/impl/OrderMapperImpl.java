package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.Order;
import com.tech.buddy.dto.OrderDTO;
import com.tech.buddy.mapper.OrderMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class OrderMapperImpl implements OrderMapper {
    @Override
    public Order toEntity(OrderDTO dto) {
        Order entity = new Order();
        entity.setId(dto.getId());
        entity.setAddressLine1(dto.getAddressLine1());
        entity.setAddressLine2(dto.getAddressLine2());
        entity.setCity(dto.getCity());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setPostalCode(dto.getPostalCode());
        entity.setState(dto.getState());
        entity.setCountry(dto.getCountry());
        entity.setDeliverDate(dto.getDeliverDate());
        entity.setDeliverDays(dto.getDeliverDays());
        entity.setOrderTime(dto.getOrderTime());
        entity.setPaymentMethod(dto.getPaymentMethod());
        entity.setProductCost(dto.getProductCost());
        entity.setShippingCost(dto.getShippingCost());
        entity.setStatus(dto.getStatus());
        entity.setSubtotal(dto.getSubtotal());
        entity.setTax(dto.getTax());
        entity.setTotal(dto.getTotal());
        entity.setCustomer(dto.getCustomer());
        return entity;
    }

    @Override
    public OrderDTO toDto(Order entity) {
        OrderDTO dto = new OrderDTO();
        dto.setId(entity.getId());
        dto.setAddressLine1(entity.getAddressLine1());
        dto.setAddressLine2(entity.getAddressLine2());
        dto.setCity(entity.getCity());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setPostalCode(entity.getPostalCode());
        dto.setState(entity.getState());
        dto.setCountry(entity.getCountry());
        dto.setDeliverDate(entity.getDeliverDate());
        dto.setDeliverDays(entity.getDeliverDays());
        dto.setOrderTime(entity.getOrderTime());
        dto.setPaymentMethod(entity.getPaymentMethod());
        dto.setProductCost(entity.getProductCost());
        dto.setShippingCost(entity.getShippingCost());
        dto.setStatus(entity.getStatus());
        dto.setSubtotal(entity.getSubtotal());
        dto.setTax(entity.getTax());
        dto.setTotal(entity.getTotal());
        dto.setCustomer(entity.getCustomer());
        return dto;
    }

    @Override
    public List<Order> toEntityList(List<OrderDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<OrderDTO> toDtoList(List<Order> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}