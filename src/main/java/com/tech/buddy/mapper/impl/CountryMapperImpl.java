package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.Country;
import com.tech.buddy.dto.CountryDTO;
import com.tech.buddy.mapper.CountryMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class CountryMapperImpl implements CountryMapper {
    @Override
    public Country toEntity(CountryDTO dto) {
        Country entity = new Country();
        entity.setId(dto.getId());
        entity.setCode(dto.getCode());
        entity.setName(dto.getName());
        return entity;
    }

    @Override
    public CountryDTO toDto(Country entity) {
        CountryDTO dto = new CountryDTO();
        dto.setId(entity.getId());
        dto.setCode(entity.getCode());
        dto.setName(entity.getName());
        return dto;
    }

    @Override
    public List<Country> toEntityList(List<CountryDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<CountryDTO> toDtoList(List<Country> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}