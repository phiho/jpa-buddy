package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.Customer;
import com.tech.buddy.dto.CustomerDTO;
import com.tech.buddy.mapper.CustomerMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class CustomerMapperImpl implements CustomerMapper {
    @Override
    public Customer toEntity(CustomerDTO dto) {
        Customer entity = new Customer();
        entity.setId(dto.getId());
        entity.setAddressLine1(dto.getAddressLine1());
        entity.setAddressLine2(dto.getAddressLine2());
        entity.setCity(dto.getCity());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setPostalCode(dto.getPostalCode());
        entity.setState(dto.getState());
        entity.setAuthenticationType(dto.getAuthenticationType());
        entity.setCreatedTime(dto.getCreatedTime());
        entity.setEmail(dto.getEmail());
        entity.setEnabled(dto.getEnabled());
        entity.setPassword(dto.getPassword());
        entity.setResetPasswordToken(dto.getResetPasswordToken());
        entity.setVerificationCode(dto.getVerificationCode());
        entity.setCountry(dto.getCountry());
        return entity;
    }

    @Override
    public CustomerDTO toDto(Customer entity) {
        CustomerDTO dto = new CustomerDTO();
        dto.setId(entity.getId());
        dto.setAddressLine1(entity.getAddressLine1());
        dto.setAddressLine2(entity.getAddressLine2());
        dto.setCity(entity.getCity());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setPostalCode(entity.getPostalCode());
        dto.setState(entity.getState());
        dto.setAuthenticationType(entity.getAuthenticationType());
        dto.setCreatedTime(entity.getCreatedTime());
        dto.setEmail(entity.getEmail());
        dto.setEnabled(entity.getEnabled());
        dto.setPassword(entity.getPassword());
        dto.setResetPasswordToken(entity.getResetPasswordToken());
        dto.setVerificationCode(entity.getVerificationCode());
        dto.setCountry(entity.getCountry());
        return dto;
    }

    @Override
    public List<Customer> toEntityList(List<CustomerDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<CustomerDTO> toDtoList(List<Customer> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}