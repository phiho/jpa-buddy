package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.Product;
import com.tech.buddy.dto.ProductDTO;
import com.tech.buddy.mapper.ProductMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class ProductMapperImpl implements ProductMapper {
    @Override
    public Product toEntity(ProductDTO dto) {
        Product entity = new Product();
        entity.setId(dto.getId());
        entity.setAlias(dto.getAlias());
        entity.setAverageRating(dto.getAverageRating());
        entity.setCost(dto.getCost());
        entity.setCreatedTime(dto.getCreatedTime());
        entity.setDiscountPercent(dto.getDiscountPercent());
        entity.setEnabled(dto.getEnabled());
        entity.setFullDescription(dto.getFullDescription());
        entity.setHeight(dto.getHeight());
        entity.setInStock(dto.getInStock());
        entity.setLength(dto.getLength());
        entity.setMainImage(dto.getMainImage());
        entity.setName(dto.getName());
        entity.setPrice(dto.getPrice());
        entity.setReviewCount(dto.getReviewCount());
        entity.setShortDescription(dto.getShortDescription());
        entity.setUpdatedTime(dto.getUpdatedTime());
        entity.setWeight(dto.getWeight());
        entity.setWidth(dto.getWidth());
        entity.setBrand(dto.getBrand());
        entity.setCategory(dto.getCategory());
        return entity;
    }

    @Override
    public ProductDTO toDto(Product entity) {
        ProductDTO dto = new ProductDTO();
        dto.setId(entity.getId());
        dto.setAlias(entity.getAlias());
        dto.setAverageRating(entity.getAverageRating());
        dto.setCost(entity.getCost());
        dto.setCreatedTime(entity.getCreatedTime());
        dto.setDiscountPercent(entity.getDiscountPercent());
        dto.setEnabled(entity.getEnabled());
        dto.setFullDescription(entity.getFullDescription());
        dto.setHeight(entity.getHeight());
        dto.setInStock(entity.getInStock());
        dto.setLength(entity.getLength());
        dto.setMainImage(entity.getMainImage());
        dto.setName(entity.getName());
        dto.setPrice(entity.getPrice());
        dto.setReviewCount(entity.getReviewCount());
        dto.setShortDescription(entity.getShortDescription());
        dto.setUpdatedTime(entity.getUpdatedTime());
        dto.setWeight(entity.getWeight());
        dto.setWidth(entity.getWidth());
        dto.setBrand(entity.getBrand());
        dto.setCategory(entity.getCategory());
        return dto;
    }

    @Override
    public List<Product> toEntityList(List<ProductDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<ProductDTO> toDtoList(List<Product> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}