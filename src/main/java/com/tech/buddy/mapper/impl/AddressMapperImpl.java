package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.Address;
import com.tech.buddy.dto.AddressDTO;
import com.tech.buddy.mapper.AddressMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class AddressMapperImpl implements AddressMapper {
    @Override
    public Address toEntity(AddressDTO dto) {
        Address entity = new Address();
        entity.setId(dto.getId());
        entity.setAddressLine1(dto.getAddressLine1());
        entity.setAddressLine2(dto.getAddressLine2());
        entity.setCity(dto.getCity());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setPhoneNumber(dto.getPhoneNumber());
        entity.setPostalCode(dto.getPostalCode());
        entity.setState(dto.getState());
        entity.setDefaultAddress(dto.getDefaultAddress());
        entity.setCountry(dto.getCountry());
        entity.setCustomer(dto.getCustomer());
        return entity;
    }

    @Override
    public AddressDTO toDto(Address entity) {
        AddressDTO dto = new AddressDTO();
        dto.setId(entity.getId());
        dto.setAddressLine1(entity.getAddressLine1());
        dto.setAddressLine2(entity.getAddressLine2());
        dto.setCity(entity.getCity());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setPostalCode(entity.getPostalCode());
        dto.setState(entity.getState());
        dto.setDefaultAddress(entity.getDefaultAddress());
        dto.setCountry(entity.getCountry());
        dto.setCustomer(entity.getCustomer());
        return dto;
    }

    @Override
    public List<Address> toEntityList(List<AddressDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<AddressDTO> toDtoList(List<Address> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}