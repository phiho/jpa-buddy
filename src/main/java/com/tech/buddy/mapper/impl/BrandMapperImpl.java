package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.Brand;
import com.tech.buddy.dto.BrandDTO;
import com.tech.buddy.mapper.BrandMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class BrandMapperImpl implements BrandMapper {
    @Override
    public Brand toEntity(BrandDTO dto) {
        Brand entity = new Brand();
        entity.setId(dto.getId());
        entity.setLogo(dto.getLogo());
        entity.setName(dto.getName());
        return entity;
    }

    @Override
    public BrandDTO toDto(Brand entity) {
        BrandDTO dto = new BrandDTO();
        dto.setId(entity.getId());
        dto.setLogo(entity.getLogo());
        dto.setName(entity.getName());
        return dto;
    }

    @Override
    public List<Brand> toEntityList(List<BrandDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<BrandDTO> toDtoList(List<Brand> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}