package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.Currency;
import com.tech.buddy.dto.CurrencyDTO;
import com.tech.buddy.mapper.CurrencyMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class CurrencyMapperImpl implements CurrencyMapper {
    @Override
    public Currency toEntity(CurrencyDTO dto) {
        Currency entity = new Currency();
        entity.setId(dto.getId());
        entity.setCode(dto.getCode());
        entity.setName(dto.getName());
        entity.setSymbol(dto.getSymbol());
        return entity;
    }

    @Override
    public CurrencyDTO toDto(Currency entity) {
        CurrencyDTO dto = new CurrencyDTO();
        dto.setId(entity.getId());
        dto.setCode(entity.getCode());
        dto.setName(entity.getName());
        dto.setSymbol(entity.getSymbol());
        return dto;
    }

    @Override
    public List<Currency> toEntityList(List<CurrencyDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<CurrencyDTO> toDtoList(List<Currency> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}