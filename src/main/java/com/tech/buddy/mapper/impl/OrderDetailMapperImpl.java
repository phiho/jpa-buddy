package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.OrderDetail;
import com.tech.buddy.dto.OrderDetailDTO;
import com.tech.buddy.mapper.OrderDetailMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class OrderDetailMapperImpl implements OrderDetailMapper {
    @Override
    public OrderDetail toEntity(OrderDetailDTO dto) {
        OrderDetail entity = new OrderDetail();
        entity.setId(dto.getId());
        entity.setProductCost(dto.getProductCost());
        entity.setQuantity(dto.getQuantity());
        entity.setShippingCost(dto.getShippingCost());
        entity.setSubtotal(dto.getSubtotal());
        entity.setUnitPrice(dto.getUnitPrice());
        entity.setOrder(dto.getOrder());
        entity.setProduct(dto.getProduct());
        return entity;
    }

    @Override
    public OrderDetailDTO toDto(OrderDetail entity) {
        OrderDetailDTO dto = new OrderDetailDTO();
        dto.setId(entity.getId());
        dto.setProductCost(entity.getProductCost());
        dto.setQuantity(entity.getQuantity());
        dto.setShippingCost(entity.getShippingCost());
        dto.setSubtotal(entity.getSubtotal());
        dto.setUnitPrice(entity.getUnitPrice());
        dto.setOrder(entity.getOrder());
        dto.setProduct(entity.getProduct());
        return dto;
    }

    @Override
    public List<OrderDetail> toEntityList(List<OrderDetailDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<OrderDetailDTO> toDtoList(List<OrderDetail> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}