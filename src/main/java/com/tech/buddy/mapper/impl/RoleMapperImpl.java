package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.Role;
import com.tech.buddy.dto.RoleDTO;
import com.tech.buddy.mapper.RoleMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class RoleMapperImpl implements RoleMapper {
    @Override
    public Role toEntity(RoleDTO dto) {
        Role entity = new Role();
        entity.setId(dto.getId());
        entity.setDescription(dto.getDescription());
        entity.setName(dto.getName());
        return entity;
    }

    @Override
    public RoleDTO toDto(Role entity) {
        RoleDTO dto = new RoleDTO();
        dto.setId(entity.getId());
        dto.setDescription(entity.getDescription());
        dto.setName(entity.getName());
        return dto;
    }

    @Override
    public List<Role> toEntityList(List<RoleDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<RoleDTO> toDtoList(List<Role> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}