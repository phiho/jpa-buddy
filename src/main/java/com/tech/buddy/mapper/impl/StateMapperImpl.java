package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.State;
import com.tech.buddy.dto.StateDTO;
import com.tech.buddy.mapper.StateMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class StateMapperImpl implements StateMapper {
    @Override
    public State toEntity(StateDTO dto) {
        State entity = new State();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setCountry(dto.getCountry());
        return entity;
    }

    @Override
    public StateDTO toDto(State entity) {
        StateDTO dto = new StateDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setCountry(entity.getCountry());
        return dto;
    }

    @Override
    public List<State> toEntityList(List<StateDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<StateDTO> toDtoList(List<State> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}