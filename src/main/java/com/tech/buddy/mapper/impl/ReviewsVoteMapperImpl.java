package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.ReviewsVote;
import com.tech.buddy.dto.ReviewsVoteDTO;
import com.tech.buddy.mapper.ReviewsVoteMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class ReviewsVoteMapperImpl implements ReviewsVoteMapper {
    @Override
    public ReviewsVote toEntity(ReviewsVoteDTO dto) {
        ReviewsVote entity = new ReviewsVote();
        entity.setId(dto.getId());
        entity.setVotes(dto.getVotes());
        entity.setCustomer(dto.getCustomer());
        entity.setReview(dto.getReview());
        return entity;
    }

    @Override
    public ReviewsVoteDTO toDto(ReviewsVote entity) {
        ReviewsVoteDTO dto = new ReviewsVoteDTO();
        dto.setId(entity.getId());
        dto.setVotes(entity.getVotes());
        dto.setCustomer(entity.getCustomer());
        dto.setReview(entity.getReview());
        return dto;
    }

    @Override
    public List<ReviewsVote> toEntityList(List<ReviewsVoteDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<ReviewsVoteDTO> toDtoList(List<ReviewsVote> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}