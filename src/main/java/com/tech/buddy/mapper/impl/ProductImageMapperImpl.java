package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.ProductImage;
import com.tech.buddy.dto.ProductImageDTO;
import com.tech.buddy.mapper.ProductImageMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class ProductImageMapperImpl implements ProductImageMapper {
    @Override
    public ProductImage toEntity(ProductImageDTO dto) {
        ProductImage entity = new ProductImage();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setProduct(dto.getProduct());
        return entity;
    }

    @Override
    public ProductImageDTO toDto(ProductImage entity) {
        ProductImageDTO dto = new ProductImageDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setProduct(entity.getProduct());
        return dto;
    }

    @Override
    public List<ProductImage> toEntityList(List<ProductImageDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<ProductImageDTO> toDtoList(List<ProductImage> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}