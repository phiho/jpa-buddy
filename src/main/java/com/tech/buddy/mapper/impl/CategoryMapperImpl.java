package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.Category;
import com.tech.buddy.dto.CategoryDTO;
import com.tech.buddy.mapper.CategoryMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class CategoryMapperImpl implements CategoryMapper {
    @Override
    public Category toEntity(CategoryDTO dto) {
        Category entity = new Category();
        entity.setId(dto.getId());
        entity.setAlias(dto.getAlias());
        entity.setAllParentIds(dto.getAllParentIds());
        entity.setEnabled(dto.getEnabled());
        entity.setImage(dto.getImage());
        entity.setName(dto.getName());
        entity.setParent(dto.getParent());
        return entity;
    }

    @Override
    public CategoryDTO toDto(Category entity) {
        CategoryDTO dto = new CategoryDTO();
        dto.setId(entity.getId());
        dto.setAlias(entity.getAlias());
        dto.setAllParentIds(entity.getAllParentIds());
        dto.setEnabled(entity.getEnabled());
        dto.setImage(entity.getImage());
        dto.setName(entity.getName());
        dto.setParent(entity.getParent());
        return dto;
    }

    @Override
    public List<Category> toEntityList(List<CategoryDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<CategoryDTO> toDtoList(List<Category> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}