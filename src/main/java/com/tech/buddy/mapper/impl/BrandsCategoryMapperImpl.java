package com.tech.buddy.mapper.impl;

import com.tech.buddy.domain.BrandsCategory;
import com.tech.buddy.dto.BrandsCategoryDTO;
import com.tech.buddy.mapper.BrandsCategoryMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;
import java.util.stream.Collectors;

public class BrandsCategoryMapperImpl implements BrandsCategoryMapper {
    @Override
    public BrandsCategory toEntity(BrandsCategoryDTO dto) {
        BrandsCategory entity = new BrandsCategory();
        entity.setId(dto.getId());
        entity.setBrand(dto.getBrand());
        entity.setCategory(dto.getCategory());
        return entity;
    }

    @Override
    public BrandsCategoryDTO toDto(BrandsCategory entity) {
        BrandsCategoryDTO dto = new BrandsCategoryDTO();
        dto.setId(entity.getId());
        dto.setBrand(entity.getBrand());
        dto.setCategory(entity.getCategory());
        return dto;
    }

    @Override
    public List<BrandsCategory> toEntityList(List<BrandsCategoryDTO> dtoList) {
        return dtoList.stream().map(this::toEntity).collect(Collectors.toList());
    }

    @Override
    public List<BrandsCategoryDTO> toDtoList(List<BrandsCategory> entityList) {
        return entityList.stream().map(this::toDto).collect(Collectors.toList());
    }
}