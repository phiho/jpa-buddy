package com.tech.buddy.mapper;

import com.tech.buddy.domain.Setting;
import com.tech.buddy.dto.SettingDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SettingMapper extends EntityMapper<SettingDTO, Setting> {
}