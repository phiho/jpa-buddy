package com.tech.buddy.mapper;

import com.tech.buddy.domain.ReviewsVote;
import com.tech.buddy.dto.ReviewsVoteDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReviewsVoteMapper extends EntityMapper<ReviewsVoteDTO, ReviewsVote> {
}