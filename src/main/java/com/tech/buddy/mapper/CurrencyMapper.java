package com.tech.buddy.mapper;

import com.tech.buddy.domain.Currency;
import com.tech.buddy.dto.CurrencyDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CurrencyMapper extends EntityMapper<CurrencyDTO, Currency> {
}