package com.tech.buddy.mapper;

import com.tech.buddy.domain.Review;
import com.tech.buddy.dto.ReviewDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ReviewMapper extends EntityMapper<ReviewDTO, Review> {
}