package com.tech.buddy.mapper;

import com.tech.buddy.domain.Category;
import com.tech.buddy.dto.CategoryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CategoryMapper extends EntityMapper<CategoryDTO, Category> {
}