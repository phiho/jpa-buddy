package com.tech.buddy.mapper;

import com.tech.buddy.domain.OrderDetail;
import com.tech.buddy.dto.OrderDetailDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderDetailMapper extends EntityMapper<OrderDetailDTO, OrderDetail> {
}