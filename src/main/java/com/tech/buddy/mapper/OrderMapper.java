package com.tech.buddy.mapper;

import com.tech.buddy.domain.Order;
import com.tech.buddy.dto.OrderDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper extends EntityMapper<OrderDTO, Order> {
}