package com.tech.buddy.mapper;

import com.tech.buddy.domain.ShippingRate;
import com.tech.buddy.dto.ShippingRateDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ShippingRateMapper extends EntityMapper<ShippingRateDTO, ShippingRate> {
}