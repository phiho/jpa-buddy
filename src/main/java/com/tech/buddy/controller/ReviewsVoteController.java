package com.tech.buddy.controller;

import com.tech.buddy.dto.ReviewsVoteDTO;
import com.tech.buddy.service.ReviewsVoteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/reviews-vote")
@RestController
@Api(tags = "ReviewsVote API")
public class ReviewsVoteController {
    private final ReviewsVoteService reviewsVoteService;

    public ReviewsVoteController(ReviewsVoteService reviewsVoteService) {
        this.reviewsVoteService = reviewsVoteService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody ReviewsVoteDTO reviewsVote) {
        reviewsVoteService.save(reviewsVote);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public ReviewsVoteDTO findById(@PathVariable("id") Integer id) {
        Optional<ReviewsVoteDTO> dtoOptional = reviewsVoteService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        reviewsVoteService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<ReviewsVoteDTO> list() {
        return reviewsVoteService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<ReviewsVoteDTO> pageQuery(Pageable pageable) {
        return reviewsVoteService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public ReviewsVoteDTO update(@RequestBody ReviewsVoteDTO dto) {
        return reviewsVoteService.updateById(dto);
    }
}