package com.tech.buddy.controller;

import com.tech.buddy.dto.ProductDetailDTO;
import com.tech.buddy.service.ProductDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/product-detail")
@RestController
@Api(tags = "ProductDetail API")
public class ProductDetailController {
    private final ProductDetailService productDetailService;

    public ProductDetailController(ProductDetailService productDetailService) {
        this.productDetailService = productDetailService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody ProductDetailDTO productDetail) {
        productDetailService.save(productDetail);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public ProductDetailDTO findById(@PathVariable("id") Integer id) {
        Optional<ProductDetailDTO> dtoOptional = productDetailService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        productDetailService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<ProductDetailDTO> list() {
        return productDetailService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<ProductDetailDTO> pageQuery(Pageable pageable) {
        return productDetailService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public ProductDetailDTO update(@RequestBody ProductDetailDTO dto) {
        return productDetailService.updateById(dto);
    }
}