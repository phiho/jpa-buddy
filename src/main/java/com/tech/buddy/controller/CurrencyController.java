package com.tech.buddy.controller;

import com.tech.buddy.dto.CurrencyDTO;
import com.tech.buddy.service.CurrencyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/currency")
@RestController
@Api(tags = "Currency API")
public class CurrencyController {
    private final CurrencyService currencyService;

    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody CurrencyDTO currency) {
        currencyService.save(currency);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public CurrencyDTO findById(@PathVariable("id") Integer id) {
        Optional<CurrencyDTO> dtoOptional = currencyService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        currencyService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<CurrencyDTO> list() {
        return currencyService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<CurrencyDTO> pageQuery(Pageable pageable) {
        return currencyService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public CurrencyDTO update(@RequestBody CurrencyDTO dto) {
        return currencyService.updateById(dto);
    }
}