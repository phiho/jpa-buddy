package com.tech.buddy.controller;

import com.tech.buddy.dto.OrderDetailDTO;
import com.tech.buddy.service.OrderDetailService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/order-detail")
@RestController
@Api(tags = "OrderDetail API")
public class OrderDetailController {
    private final OrderDetailService orderDetailService;

    public OrderDetailController(OrderDetailService orderDetailService) {
        this.orderDetailService = orderDetailService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody OrderDetailDTO orderDetail) {
        orderDetailService.save(orderDetail);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public OrderDetailDTO findById(@PathVariable("id") Integer id) {
        Optional<OrderDetailDTO> dtoOptional = orderDetailService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        orderDetailService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<OrderDetailDTO> list() {
        return orderDetailService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<OrderDetailDTO> pageQuery(Pageable pageable) {
        return orderDetailService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public OrderDetailDTO update(@RequestBody OrderDetailDTO dto) {
        return orderDetailService.updateById(dto);
    }
}