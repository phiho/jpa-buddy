package com.tech.buddy.controller;

import com.tech.buddy.dto.CountryDTO;
import com.tech.buddy.service.CountryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/country")
@RestController
@Api(tags = "Country API")
public class CountryController {
    private final CountryService countryService;

    public CountryController(CountryService countryService) {
        this.countryService = countryService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody CountryDTO country) {
        countryService.save(country);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public CountryDTO findById(@PathVariable("id") Integer id) {
        Optional<CountryDTO> dtoOptional = countryService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        countryService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<CountryDTO> list() {
        return countryService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<CountryDTO> pageQuery(Pageable pageable) {
        return countryService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public CountryDTO update(@RequestBody CountryDTO dto) {
        return countryService.updateById(dto);
    }
}