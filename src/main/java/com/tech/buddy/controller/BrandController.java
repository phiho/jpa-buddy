package com.tech.buddy.controller;

import com.tech.buddy.dto.BrandDTO;
import com.tech.buddy.service.BrandService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/brand")
@RestController
@Api(tags = "Brand API")
public class BrandController {
    private final BrandService brandService;

    public BrandController(BrandService brandService) {
        this.brandService = brandService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody BrandDTO brand) {
        brandService.save(brand);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public BrandDTO findById(@PathVariable("id") Integer id) {
        Optional<BrandDTO> dtoOptional = brandService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        brandService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<BrandDTO> list() {
        return brandService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<BrandDTO> pageQuery(Pageable pageable) {
        return brandService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public BrandDTO update(@RequestBody BrandDTO dto) {
        return brandService.updateById(dto);
    }
}