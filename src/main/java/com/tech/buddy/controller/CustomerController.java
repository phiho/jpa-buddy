package com.tech.buddy.controller;

import com.tech.buddy.dto.CustomerDTO;
import com.tech.buddy.service.CustomerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/customer")
@RestController
@Api(tags = "Customer API")
public class CustomerController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody CustomerDTO customer) {
        customerService.save(customer);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public CustomerDTO findById(@PathVariable("id") Integer id) {
        Optional<CustomerDTO> dtoOptional = customerService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        customerService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<CustomerDTO> list() {
        return customerService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<CustomerDTO> pageQuery(Pageable pageable) {
        return customerService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public CustomerDTO update(@RequestBody CustomerDTO dto) {
        return customerService.updateById(dto);
    }
}