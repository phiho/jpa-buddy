package com.tech.buddy.controller;

import com.tech.buddy.domain.BrandsCategoryId;
import com.tech.buddy.dto.BrandsCategoryDTO;
import com.tech.buddy.service.BrandsCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/brands-category")
@RestController
@Api(tags = "BrandsCategory API")
public class BrandsCategoryController {
    private final BrandsCategoryService brandsCategoryService;

    public BrandsCategoryController(BrandsCategoryService brandsCategoryService) {
        this.brandsCategoryService = brandsCategoryService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody BrandsCategoryDTO brandsCategory) {
        brandsCategoryService.save(brandsCategory);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public BrandsCategoryDTO findById(@PathVariable("id") BrandsCategoryId id) {
        Optional<BrandsCategoryDTO> dtoOptional = brandsCategoryService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") BrandsCategoryId id) {
        brandsCategoryService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<BrandsCategoryDTO> list() {
        return brandsCategoryService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<BrandsCategoryDTO> pageQuery(Pageable pageable) {
        return brandsCategoryService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public BrandsCategoryDTO update(@RequestBody BrandsCategoryDTO dto) {
        return brandsCategoryService.updateById(dto);
    }
}