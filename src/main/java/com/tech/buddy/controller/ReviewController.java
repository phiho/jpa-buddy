package com.tech.buddy.controller;

import com.tech.buddy.dto.ReviewDTO;
import com.tech.buddy.service.ReviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/review")
@RestController
@Api(tags = "Review API")
public class ReviewController {
    private final ReviewService reviewService;

    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody ReviewDTO review) {
        reviewService.save(review);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public ReviewDTO findById(@PathVariable("id") Integer id) {
        Optional<ReviewDTO> dtoOptional = reviewService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        reviewService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<ReviewDTO> list() {
        return reviewService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<ReviewDTO> pageQuery(Pageable pageable) {
        return reviewService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public ReviewDTO update(@RequestBody ReviewDTO dto) {
        return reviewService.updateById(dto);
    }
}