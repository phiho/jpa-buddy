package com.tech.buddy.controller;

import com.tech.buddy.dto.CartItemDTO;
import com.tech.buddy.service.CartItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/cart-item")
@RestController
@Api(tags = "CartItem API")
public class CartItemController {
    private final CartItemService cartItemService;

    public CartItemController(CartItemService cartItemService) {
        this.cartItemService = cartItemService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody CartItemDTO cartItem) {
        cartItemService.save(cartItem);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public CartItemDTO findById(@PathVariable("id") Integer id) {
        Optional<CartItemDTO> dtoOptional = cartItemService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        cartItemService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<CartItemDTO> list() {
        return cartItemService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<CartItemDTO> pageQuery(Pageable pageable) {
        return cartItemService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public CartItemDTO update(@RequestBody CartItemDTO dto) {
        return cartItemService.updateById(dto);
    }
}