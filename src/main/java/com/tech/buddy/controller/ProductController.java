package com.tech.buddy.controller;

import com.tech.buddy.dto.ProductDTO;
import com.tech.buddy.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/product")
@RestController
@Api(tags = "Product API")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody ProductDTO product) {
        productService.save(product);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public ProductDTO findById(@PathVariable("id") Integer id) {
        Optional<ProductDTO> dtoOptional = productService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        productService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<ProductDTO> list() {
        return productService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<ProductDTO> pageQuery(Pageable pageable) {
        return productService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public ProductDTO update(@RequestBody ProductDTO dto) {
        return productService.updateById(dto);
    }
}