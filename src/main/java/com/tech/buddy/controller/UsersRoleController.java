package com.tech.buddy.controller;

import com.tech.buddy.domain.UsersRoleId;
import com.tech.buddy.dto.UsersRoleDTO;
import com.tech.buddy.service.UsersRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/users-role")
@RestController
@Api(tags = "UsersRole API")
public class UsersRoleController {
    private final UsersRoleService usersRoleService;

    public UsersRoleController(UsersRoleService usersRoleService) {
        this.usersRoleService = usersRoleService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody UsersRoleDTO usersRole) {
        usersRoleService.save(usersRole);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public UsersRoleDTO findById(@PathVariable("id") UsersRoleId id) {
        Optional<UsersRoleDTO> dtoOptional = usersRoleService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") UsersRoleId id) {
        usersRoleService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<UsersRoleDTO> list() {
        return usersRoleService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<UsersRoleDTO> pageQuery(Pageable pageable) {
        return usersRoleService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public UsersRoleDTO update(@RequestBody UsersRoleDTO dto) {
        return usersRoleService.updateById(dto);
    }
}