package com.tech.buddy.controller;

import com.tech.buddy.dto.OrderTrackDTO;
import com.tech.buddy.service.OrderTrackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/order-track")
@RestController
@Api(tags = "OrderTrack API")
public class OrderTrackController {
    private final OrderTrackService orderTrackService;

    public OrderTrackController(OrderTrackService orderTrackService) {
        this.orderTrackService = orderTrackService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody OrderTrackDTO orderTrack) {
        orderTrackService.save(orderTrack);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public OrderTrackDTO findById(@PathVariable("id") Integer id) {
        Optional<OrderTrackDTO> dtoOptional = orderTrackService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        orderTrackService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<OrderTrackDTO> list() {
        return orderTrackService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<OrderTrackDTO> pageQuery(Pageable pageable) {
        return orderTrackService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public OrderTrackDTO update(@RequestBody OrderTrackDTO dto) {
        return orderTrackService.updateById(dto);
    }
}