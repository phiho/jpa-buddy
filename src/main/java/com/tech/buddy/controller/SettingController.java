package com.tech.buddy.controller;

import com.tech.buddy.dto.SettingDTO;
import com.tech.buddy.service.SettingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/setting")
@RestController
@Api(tags = "Setting API")
public class SettingController {
    private final SettingService settingService;

    public SettingController(SettingService settingService) {
        this.settingService = settingService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody SettingDTO setting) {
        settingService.save(setting);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public SettingDTO findById(@PathVariable("id") String id) {
        Optional<SettingDTO> dtoOptional = settingService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") String id) {
        settingService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<SettingDTO> list() {
        return settingService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<SettingDTO> pageQuery(Pageable pageable) {
        return settingService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public SettingDTO update(@RequestBody SettingDTO dto) {
        return settingService.updateById(dto);
    }
}