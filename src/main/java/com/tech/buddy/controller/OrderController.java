package com.tech.buddy.controller;

import com.tech.buddy.dto.OrderDTO;
import com.tech.buddy.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/order")
@RestController
@Api(tags = "Order API")
public class OrderController {
    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody OrderDTO order) {
        orderService.save(order);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public OrderDTO findById(@PathVariable("id") Integer id) {
        Optional<OrderDTO> dtoOptional = orderService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        orderService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<OrderDTO> list() {
        return orderService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<OrderDTO> pageQuery(Pageable pageable) {
        return orderService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public OrderDTO update(@RequestBody OrderDTO dto) {
        return orderService.updateById(dto);
    }
}