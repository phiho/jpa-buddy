package com.tech.buddy.controller;

import com.tech.buddy.dto.AddressDTO;
import com.tech.buddy.service.AddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/api/address")
@RestController
@Api(tags = "Address API")
public class AddressController {
    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody AddressDTO address) {
        addressService.save(address);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public AddressDTO findById(@PathVariable("id") Integer id) {
        Optional<AddressDTO> dtoOptional = addressService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        addressService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<AddressDTO> list() {
        return addressService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<AddressDTO> pageQuery(Pageable pageable) {
        return addressService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public AddressDTO update(@RequestBody AddressDTO dto) {
        return addressService.updateById(dto);
    }
}