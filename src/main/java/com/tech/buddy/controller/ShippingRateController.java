package com.tech.buddy.controller;

import com.tech.buddy.dto.ShippingRateDTO;
import com.tech.buddy.service.ShippingRateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/shipping-rate")
@RestController
@Api(tags = "ShippingRate API")
public class ShippingRateController {
    private final ShippingRateService shippingRateService;

    public ShippingRateController(ShippingRateService shippingRateService) {
        this.shippingRateService = shippingRateService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody ShippingRateDTO shippingRate) {
        shippingRateService.save(shippingRate);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public ShippingRateDTO findById(@PathVariable("id") Integer id) {
        Optional<ShippingRateDTO> dtoOptional = shippingRateService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        shippingRateService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<ShippingRateDTO> list() {
        return shippingRateService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<ShippingRateDTO> pageQuery(Pageable pageable) {
        return shippingRateService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public ShippingRateDTO update(@RequestBody ShippingRateDTO dto) {
        return shippingRateService.updateById(dto);
    }
}