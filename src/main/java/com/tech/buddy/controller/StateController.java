package com.tech.buddy.controller;

import com.tech.buddy.dto.StateDTO;
import com.tech.buddy.service.StateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/state")
@RestController
@Api(tags = "State API")
public class StateController {
    private final StateService stateService;

    public StateController(StateService stateService) {
        this.stateService = stateService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody StateDTO state) {
        stateService.save(state);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public StateDTO findById(@PathVariable("id") Integer id) {
        Optional<StateDTO> dtoOptional = stateService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        stateService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<StateDTO> list() {
        return stateService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<StateDTO> pageQuery(Pageable pageable) {
        return stateService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public StateDTO update(@RequestBody StateDTO dto) {
        return stateService.updateById(dto);
    }
}