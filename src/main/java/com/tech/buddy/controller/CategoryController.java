package com.tech.buddy.controller;

import com.tech.buddy.dto.CategoryDTO;
import com.tech.buddy.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/category")
@RestController
@Api(tags = "Category API")
public class CategoryController {
    private final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody CategoryDTO category) {
        categoryService.save(category);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public CategoryDTO findById(@PathVariable("id") Integer id) {
        Optional<CategoryDTO> dtoOptional = categoryService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        categoryService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<CategoryDTO> list() {
        return categoryService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<CategoryDTO> pageQuery(Pageable pageable) {
        return categoryService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public CategoryDTO update(@RequestBody CategoryDTO dto) {
        return categoryService.updateById(dto);
    }
}