package com.tech.buddy.controller;

import com.tech.buddy.dto.ProductImageDTO;
import com.tech.buddy.service.ProductImageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RequestMapping("/product-image")
@RestController
@Api(tags = "ProductImage API")
public class ProductImageController {
    private final ProductImageService productImageService;

    public ProductImageController(ProductImageService productImageService) {
        this.productImageService = productImageService;
    }

    @ApiOperation("Add new data")
    @PostMapping("/save")
    public void save(@RequestBody ProductImageDTO productImage) {
        productImageService.save(productImage);
    }

    @ApiOperation("Delete based on primary key")
    @GetMapping("/{id}")
    public ProductImageDTO findById(@PathVariable("id") Integer id) {
        Optional<ProductImageDTO> dtoOptional = productImageService.findById(id);
        return dtoOptional.orElse(null);
    }

    @ApiOperation("Find by Id")
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") Integer id) {
        productImageService.deleteById(id);
    }

    @ApiOperation("Find all data")
    @GetMapping("/list")
    public List<ProductImageDTO> list() {
        return productImageService.findAll();
    }

    @ApiOperation("Pagination request")
    @GetMapping("/page-query")
    public Page<ProductImageDTO> pageQuery(Pageable pageable) {
        return productImageService.findAll(pageable);
    }

    @ApiOperation("Update one data")
    @PutMapping("/update/{id}")
    public ProductImageDTO update(@RequestBody ProductImageDTO dto) {
        return productImageService.updateById(dto);
    }
}