package com.tech.buddy.dao;

import com.tech.buddy.domain.ReviewsVote;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewsVoteRepository extends PagingAndSortingRepository<ReviewsVote, Integer> {
}