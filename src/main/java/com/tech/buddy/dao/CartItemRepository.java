package com.tech.buddy.dao;

import com.tech.buddy.domain.CartItem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartItemRepository extends PagingAndSortingRepository<CartItem, Integer> {
}