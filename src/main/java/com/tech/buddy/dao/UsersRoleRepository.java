package com.tech.buddy.dao;

import com.tech.buddy.domain.UsersRole;
import com.tech.buddy.domain.UsersRoleId;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRoleRepository extends PagingAndSortingRepository<UsersRole, UsersRoleId> {
}