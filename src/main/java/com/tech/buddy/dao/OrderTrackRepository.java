package com.tech.buddy.dao;

import com.tech.buddy.domain.OrderTrack;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderTrackRepository extends PagingAndSortingRepository<OrderTrack, Integer> {
}