package com.tech.buddy.dao;

import com.tech.buddy.domain.ShippingRate;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShippingRateRepository extends PagingAndSortingRepository<ShippingRate, Integer> {
}