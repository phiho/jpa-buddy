package com.tech.buddy.dao;

import com.tech.buddy.domain.ProductImage;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductImageRepository extends PagingAndSortingRepository<ProductImage, Integer> {
}