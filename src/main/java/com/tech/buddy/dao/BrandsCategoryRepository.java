package com.tech.buddy.dao;

import com.tech.buddy.domain.BrandsCategory;
import com.tech.buddy.domain.BrandsCategoryId;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandsCategoryRepository extends PagingAndSortingRepository<BrandsCategory, BrandsCategoryId> {
}