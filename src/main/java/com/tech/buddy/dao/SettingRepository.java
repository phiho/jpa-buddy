package com.tech.buddy.dao;

import com.tech.buddy.domain.Setting;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SettingRepository extends PagingAndSortingRepository<Setting, String> {
}