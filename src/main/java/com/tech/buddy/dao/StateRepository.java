package com.tech.buddy.dao;

import com.tech.buddy.domain.State;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StateRepository extends PagingAndSortingRepository<State, Integer> {
}