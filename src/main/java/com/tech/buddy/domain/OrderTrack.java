package com.tech.buddy.domain;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "order_track")
public class OrderTrack {
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "notes", length = 256)
    private String notes;

    @Column(name = "status", nullable = false, length = 45)
    private String status;

    @Column(name = "updated_time")
    private Instant updatedTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private Order order;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Instant updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

}