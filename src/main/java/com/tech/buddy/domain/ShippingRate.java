package com.tech.buddy.domain;

import javax.persistence.*;

@Entity
@Table(name = "shipping_rates")
public class ShippingRate {
    @Id
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "cod_supported")
    private Boolean codSupported;

    @Column(name = "days", nullable = false)
    private Integer days;

    @Column(name = "rate", nullable = false)
    private Float rate;

    @Column(name = "state", nullable = false, length = 45)
    private String state;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    private Country country;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getCodSupported() {
        return codSupported;
    }

    public void setCodSupported(Boolean codSupported) {
        this.codSupported = codSupported;
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

}