package com.tech.buddy.domain;

import javax.persistence.*;

@Entity
@Table(name = "brands_categories")
public class BrandsCategory {
    @EmbeddedId
    private BrandsCategoryId id;

    @MapsId("brandId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "brand_id", nullable = false)
    private Brand brand;

    @MapsId("categoryId")
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "category_id", nullable = false)
    private Category category;

    public BrandsCategoryId getId() {
        return id;
    }

    public void setId(BrandsCategoryId id) {
        this.id = id;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

}