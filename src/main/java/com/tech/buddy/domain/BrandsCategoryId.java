package com.tech.buddy.domain;

import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class BrandsCategoryId implements Serializable {
    private static final long serialVersionUID = 2768528604753077484L;
    @Column(name = "brand_id", nullable = false)
    private Integer brandId;

    @Column(name = "category_id", nullable = false)
    private Integer categoryId;

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        BrandsCategoryId entity = (BrandsCategoryId) o;
        return Objects.equals(this.brandId, entity.brandId) &&
                Objects.equals(this.categoryId, entity.categoryId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brandId, categoryId);
    }

}