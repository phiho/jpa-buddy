package com.tech.buddy.dto;

import com.tech.buddy.domain.Country;
import org.springframework.format.annotation.DateTimeFormat;

public class StateDTO {
    private Integer id;
    private String name;
    private Country country;

    public StateDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Country getCountry() {
        return this.country;
    }
}