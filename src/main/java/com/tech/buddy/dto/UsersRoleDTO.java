package com.tech.buddy.dto;

import com.tech.buddy.domain.Role;
import com.tech.buddy.domain.User;
import com.tech.buddy.domain.UsersRoleId;
import org.springframework.format.annotation.DateTimeFormat;

public class UsersRoleDTO {
    private UsersRoleId id;
    private User user;
    private Role role;

    public UsersRoleDTO() {
    }

    public void setId(UsersRoleId id) {
        this.id = id;
    }

    public UsersRoleId getId() {
        return this.id;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return this.user;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Role getRole() {
        return this.role;
    }
}