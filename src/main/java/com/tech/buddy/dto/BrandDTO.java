package com.tech.buddy.dto;

import org.springframework.format.annotation.DateTimeFormat;

public class BrandDTO {
    private Integer id;
    private String logo;
    private String name;

    public BrandDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getLogo() {
        return this.logo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}