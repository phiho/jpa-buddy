package com.tech.buddy.dto;

import com.tech.buddy.domain.Product;
import org.springframework.format.annotation.DateTimeFormat;

public class ProductDetailDTO {
    private Integer id;
    private String name;
    private String value;
    private Product product;

    public ProductDetailDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return this.product;
    }
}