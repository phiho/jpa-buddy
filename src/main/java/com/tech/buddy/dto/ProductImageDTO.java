package com.tech.buddy.dto;

import com.tech.buddy.domain.Product;
import org.springframework.format.annotation.DateTimeFormat;

public class ProductImageDTO {
    private Integer id;
    private String name;
    private Product product;

    public ProductImageDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return this.product;
    }
}