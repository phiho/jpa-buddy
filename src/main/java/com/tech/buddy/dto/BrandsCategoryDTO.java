package com.tech.buddy.dto;

import com.tech.buddy.domain.Brand;
import com.tech.buddy.domain.BrandsCategoryId;
import com.tech.buddy.domain.Category;
import org.springframework.format.annotation.DateTimeFormat;

public class BrandsCategoryDTO {
    private BrandsCategoryId id;
    private Brand brand;
    private Category category;

    public BrandsCategoryDTO() {
    }

    public void setId(BrandsCategoryId id) {
        this.id = id;
    }

    public BrandsCategoryId getId() {
        return this.id;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Brand getBrand() {
        return this.brand;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return this.category;
    }
}