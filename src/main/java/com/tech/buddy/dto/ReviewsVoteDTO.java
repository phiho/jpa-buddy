package com.tech.buddy.dto;

import com.tech.buddy.domain.Customer;
import com.tech.buddy.domain.Review;
import org.springframework.format.annotation.DateTimeFormat;

public class ReviewsVoteDTO {
    private Integer id;
    private Integer votes;
    private Customer customer;
    private Review review;

    public ReviewsVoteDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setVotes(Integer votes) {
        this.votes = votes;
    }

    public Integer getVotes() {
        return this.votes;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setReview(Review review) {
        this.review = review;
    }

    public Review getReview() {
        return this.review;
    }
}