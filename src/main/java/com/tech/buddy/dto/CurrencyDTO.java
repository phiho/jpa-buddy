package com.tech.buddy.dto;

import org.springframework.format.annotation.DateTimeFormat;

public class CurrencyDTO {
    private Integer id;
    private String code;
    private String name;
    private String symbol;

    public CurrencyDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return this.symbol;
    }
}