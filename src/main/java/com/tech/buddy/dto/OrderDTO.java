package com.tech.buddy.dto;

import com.tech.buddy.domain.Customer;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.Instant;

public class OrderDTO {
    private Integer id;
    private String addressLine1;
    private String addressLine2;
    private String city;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String postalCode;
    private String state;
    private String country;
    private Instant deliverDate;
    private Integer deliverDays;
    private Instant orderTime;
    private String paymentMethod;
    private Float productCost;
    private Float shippingCost;
    private String status;
    private Float subtotal;
    private Float tax;
    private Float total;
    private Customer customer;

    public OrderDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine1() {
        return this.addressLine1;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine2() {
        return this.addressLine2;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCity() {
        return this.city;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCode() {
        return this.postalCode;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountry() {
        return this.country;
    }

    public void setDeliverDate(Instant deliverDate) {
        this.deliverDate = deliverDate;
    }

    public Instant getDeliverDate() {
        return this.deliverDate;
    }

    public void setDeliverDays(Integer deliverDays) {
        this.deliverDays = deliverDays;
    }

    public Integer getDeliverDays() {
        return this.deliverDays;
    }

    public void setOrderTime(Instant orderTime) {
        this.orderTime = orderTime;
    }

    public Instant getOrderTime() {
        return this.orderTime;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethod() {
        return this.paymentMethod;
    }

    public void setProductCost(Float productCost) {
        this.productCost = productCost;
    }

    public Float getProductCost() {
        return this.productCost;
    }

    public void setShippingCost(Float shippingCost) {
        this.shippingCost = shippingCost;
    }

    public Float getShippingCost() {
        return this.shippingCost;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setSubtotal(Float subtotal) {
        this.subtotal = subtotal;
    }

    public Float getSubtotal() {
        return this.subtotal;
    }

    public void setTax(Float tax) {
        this.tax = tax;
    }

    public Float getTax() {
        return this.tax;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public Float getTotal() {
        return this.total;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return this.customer;
    }
}