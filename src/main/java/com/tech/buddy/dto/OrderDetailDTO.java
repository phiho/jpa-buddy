package com.tech.buddy.dto;

import com.tech.buddy.domain.Order;
import com.tech.buddy.domain.Product;
import org.springframework.format.annotation.DateTimeFormat;

public class OrderDetailDTO {
    private Integer id;
    private Float productCost;
    private Integer quantity;
    private Float shippingCost;
    private Float subtotal;
    private Float unitPrice;
    private Order order;
    private Product product;

    public OrderDetailDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setProductCost(Float productCost) {
        this.productCost = productCost;
    }

    public Float getProductCost() {
        return this.productCost;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setShippingCost(Float shippingCost) {
        this.shippingCost = shippingCost;
    }

    public Float getShippingCost() {
        return this.shippingCost;
    }

    public void setSubtotal(Float subtotal) {
        this.subtotal = subtotal;
    }

    public Float getSubtotal() {
        return this.subtotal;
    }

    public void setUnitPrice(Float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Float getUnitPrice() {
        return this.unitPrice;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return this.order;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return this.product;
    }
}