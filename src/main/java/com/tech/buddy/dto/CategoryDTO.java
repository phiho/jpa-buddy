package com.tech.buddy.dto;

import com.tech.buddy.domain.Category;
import org.springframework.format.annotation.DateTimeFormat;

public class CategoryDTO {
    private Integer id;
    private String alias;
    private String allParentIds;
    private Boolean enabled;
    private String image;
    private String name;
    private Category parent;

    public CategoryDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAllParentIds(String allParentIds) {
        this.allParentIds = allParentIds;
    }

    public String getAllParentIds() {
        return this.allParentIds;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return this.image;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setParent(Category parent) {
        this.parent = parent;
    }

    public Category getParent() {
        return this.parent;
    }
}