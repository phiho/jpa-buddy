package com.tech.buddy.dto;

import org.springframework.format.annotation.DateTimeFormat;

public class CountryDTO {
    private Integer id;
    private String code;
    private String name;

    public CountryDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}