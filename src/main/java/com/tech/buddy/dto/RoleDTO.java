package com.tech.buddy.dto;

import org.springframework.format.annotation.DateTimeFormat;

public class RoleDTO {
    private Integer id;
    private String description;
    private String name;

    public RoleDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return this.description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}