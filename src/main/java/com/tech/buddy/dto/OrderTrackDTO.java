package com.tech.buddy.dto;

import com.tech.buddy.domain.Order;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.Instant;

public class OrderTrackDTO {
    private Integer id;
    private String notes;
    private String status;
    private Instant updatedTime;
    private Order order;

    public OrderTrackDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return this.notes;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setUpdatedTime(Instant updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Instant getUpdatedTime() {
        return this.updatedTime;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return this.order;
    }
}