package com.tech.buddy.dto;

import org.springframework.format.annotation.DateTimeFormat;

public class SettingDTO {
    private String id;
    private String category;
    private String value;

    public SettingDTO() {
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategory() {
        return this.category;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}