package com.tech.buddy.dto;

import com.tech.buddy.domain.Country;
import org.springframework.format.annotation.DateTimeFormat;

public class ShippingRateDTO {
    private Integer id;
    private Boolean codSupported;
    private Integer days;
    private Float rate;
    private String state;
    private Country country;

    public ShippingRateDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setCodSupported(Boolean codSupported) {
        this.codSupported = codSupported;
    }

    public Boolean getCodSupported() {
        return this.codSupported;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Integer getDays() {
        return this.days;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public Float getRate() {
        return this.rate;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return this.state;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Country getCountry() {
        return this.country;
    }
}