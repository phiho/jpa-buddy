package com.tech.buddy.dto;

import com.tech.buddy.domain.Brand;
import com.tech.buddy.domain.Category;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.Instant;

public class ProductDTO {
    private Integer id;
    private String alias;
    private Float averageRating;
    private Float cost;
    private Instant createdTime;
    private Float discountPercent;
    private Boolean enabled;
    private String fullDescription;
    private Float height;
    private Boolean inStock;
    private Float length;
    private String mainImage;
    private String name;
    private Float price;
    private Integer reviewCount;
    private String shortDescription;
    private Instant updatedTime;
    private Float weight;
    private Float width;
    private Brand brand;
    private Category category;

    public ProductDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAverageRating(Float averageRating) {
        this.averageRating = averageRating;
    }

    public Float getAverageRating() {
        return this.averageRating;
    }

    public void setCost(Float cost) {
        this.cost = cost;
    }

    public Float getCost() {
        return this.cost;
    }

    public void setCreatedTime(Instant createdTime) {
        this.createdTime = createdTime;
    }

    public Instant getCreatedTime() {
        return this.createdTime;
    }

    public void setDiscountPercent(Float discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Float getDiscountPercent() {
        return this.discountPercent;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getEnabled() {
        return this.enabled;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getFullDescription() {
        return this.fullDescription;
    }

    public void setHeight(Float height) {
        this.height = height;
    }

    public Float getHeight() {
        return this.height;
    }

    public void setInStock(Boolean inStock) {
        this.inStock = inStock;
    }

    public Boolean getInStock() {
        return this.inStock;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public Float getLength() {
        return this.length;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public String getMainImage() {
        return this.mainImage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Float getPrice() {
        return this.price;
    }

    public void setReviewCount(Integer reviewCount) {
        this.reviewCount = reviewCount;
    }

    public Integer getReviewCount() {
        return this.reviewCount;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getShortDescription() {
        return this.shortDescription;
    }

    public void setUpdatedTime(Instant updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Instant getUpdatedTime() {
        return this.updatedTime;
    }

    public void setWeight(Float weight) {
        this.weight = weight;
    }

    public Float getWeight() {
        return this.weight;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public Float getWidth() {
        return this.width;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Brand getBrand() {
        return this.brand;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Category getCategory() {
        return this.category;
    }
}