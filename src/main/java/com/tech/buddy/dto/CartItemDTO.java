package com.tech.buddy.dto;

import com.tech.buddy.domain.Customer;
import com.tech.buddy.domain.Product;
import org.springframework.format.annotation.DateTimeFormat;

public class CartItemDTO {
    private Integer id;
    private Integer quantity;
    private Customer customer;
    private Product product;

    public CartItemDTO() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getQuantity() {
        return this.quantity;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Customer getCustomer() {
        return this.customer;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return this.product;
    }
}